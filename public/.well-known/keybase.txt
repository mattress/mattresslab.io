==================================================================
https://keybase.io/mattress42069
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://mattress.gitlab.io
  * I am mattress42069 (https://keybase.io/mattress42069) on keybase.
  * I have a public key ASB_0PVyWoBlls5bAwd3xEu76ygdhhzc3g7JzPNSqrJGYQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120cbc2cc5ced2f804bed3b65a954a09b5f1e92ab7e9927d4411ca83c8b60c70bea0a",
      "host": "keybase.io",
      "kid": "01207fd0f5725a806596ce5b030777c44bbbeb281d861cdcde0ec9ccf352aab246610a",
      "uid": "8fd9a6eb1c314edd01dfa7b9e6cf8619",
      "username": "mattress42069"
    },
    "merkle_root": {
      "ctime": 1553238305,
      "hash": "540ceeaf1917bba3ba0ee1fb3493da5a03456cad2f08fb85ded94192cc8cb2bca86e3725e1beb84fc46b29b5a64e8a22ceed1b19cd43b57cd312e3dc838cf27a",
      "hash_meta": "35a39ade7027ff61181767d0ae327b1f623559a66100af127077e95118c2d984",
      "seqno": 5000092
    },
    "service": {
      "entropy": "NNz1lVv1JKNKZvngL+QRpsem",
      "hostname": "mattress.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.1.2"
  },
  "ctime": 1553238322,
  "expire_in": 504576000,
  "prev": "02913731505dc324c50b6238f39f88080e40a0661a36e4805fb2638a18d40eec",
  "seqno": 41,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgf9D1clqAZZbOWwMHd8RLu+soHYYc3N4OyczzUqqyRmEKp3BheWxvYWTESpcCKcQgApE3MVBdwyTFC2I485+ICA5AoGYaNuSAX7JjihjUDuzEIH83BrTBQaODOfi+Lo0rdls5esEz6RVOJxxCsVXYWy3eAgHCo3NpZ8RAeKF1DpWv/jnn0IQhS9l/HOHSS1IvnhoLktdz3ivRVw3WvqTp54PudyPu3c2GatTlVYXIeJHzPwAoes6T3tGTB6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEICom2cA2OZ0p6sjnQ8Qo8pFd1Cy7/Npyp/Yrj3rX3nhwo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/mattress42069

==================================================================